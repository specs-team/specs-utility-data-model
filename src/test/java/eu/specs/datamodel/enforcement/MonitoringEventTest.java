package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class MonitoringEventTest {

    @Test
    public void testDeserializationFromJson() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        MonitoringEvent monitoringEvent = mapper.readValue(
                this.getClass().getResource("/monitoring-event.json"),
                MonitoringEvent.class);

        assertEquals(monitoringEvent.getComponent(), "290b7267-a62b-42dd-9037-62b5e96fe719");
        assertEquals(monitoringEvent.getType(), "integer");
        assertEquals(monitoringEvent.getData(), "3");
        assertEquals(monitoringEvent.getSlaId(), "56cc642de4b0d0b2d17908e6");
        assertEquals(monitoringEvent.getMetricId(), "dDoS_scan_frequency_m16");
    }
}