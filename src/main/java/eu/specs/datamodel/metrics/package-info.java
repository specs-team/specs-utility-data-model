/**
 * Created by adispataru on 7/5/15.
 */
@XmlSchema(
        namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate",
        elementFormDefault = XmlNsForm.QUALIFIED)
package eu.specs.datamodel.metrics;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;