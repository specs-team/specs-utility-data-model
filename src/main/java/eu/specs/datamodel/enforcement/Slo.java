package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.specs.datamodel.sla.sdt.WeightType;

public class Slo {
    @JsonProperty("slo_id")
    private String id;
    @JsonProperty("capability")
    private String capability;
    @JsonProperty("metric_id")
    private String metricId;
    @JsonProperty("unit_type")
    private String unitType;
    @JsonProperty("unit")
    private String unit;
    @JsonProperty("value")
    private String value;
    @JsonProperty("operator")
    private String operator;
    @JsonProperty("value1")
    private String value1;
    @JsonProperty("value2")
    private String value2;
    @JsonProperty("interval_step")
    private String intervalStep;
    @JsonProperty("importance_level")
    private ImportanceLevel importanceLevel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCapability() {
        return capability;
    }

    public void setCapability(String capability) {
        this.capability = capability;
    }

    public String getMetricId() {
        return metricId;
    }

    public void setMetricId(String metricId) {
        this.metricId = metricId;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    public String getIntervalStep() {
        return intervalStep;
    }

    public void setIntervalStep(String intervalStep) {
        this.intervalStep = intervalStep;
    }

    public ImportanceLevel getImportanceLevel() {
        return importanceLevel;
    }

    public void setImportanceLevel(ImportanceLevel importanceLevel) {
        this.importanceLevel = importanceLevel;
    }

    public static enum ImportanceLevel {
        LOW,
        MEDIUM,
        HIGH
    }
}
