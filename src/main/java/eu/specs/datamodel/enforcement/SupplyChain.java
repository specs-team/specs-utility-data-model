package eu.specs.datamodel.enforcement;

import eu.specs.datamodel.common.Annotation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SupplyChain {
    private String id;
    private String scaId;
    private Date creationTime;
    private String slaId;
    private CloudResource cloudResource;
    private List<String> securityMechanisms;
    private int vmNumber;
    private int poolNumber;
    private List<Allocation> allocations;
    private List<Annotation> annotations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScaId() {
        return scaId;
    }

    public void setScaId(String scaId) {
        this.scaId = scaId;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public CloudResource getCloudResource() {
        return cloudResource;
    }

    public void setCloudResource(CloudResource cloudResource) {
        this.cloudResource = cloudResource;
    }

    public int getVmNumber() {
        return vmNumber;
    }

    public void setVmNumber(int vmNumber) {
        this.vmNumber = vmNumber;
    }

    public List<Allocation> getAllocations() {
        return allocations;
    }

    public void setAllocations(List<Allocation> allocations) {
        this.allocations = allocations;
    }

    public void addAllocation(Allocation allocation) {
        if (this.allocations == null) {
            this.allocations = new ArrayList<>();
        }
        this.allocations.add(allocation);
    }

    public List<String> getSecurityMechanisms() {
        return securityMechanisms;
    }

    public void setSecurityMechanisms(List<String> securityMechanisms) {
        this.securityMechanisms = securityMechanisms;
    }

    public void addSecurityMechanisms(String securityMechanism) {
        if (this.securityMechanisms == null) {
            this.securityMechanisms = new ArrayList<>();
        }
        this.securityMechanisms.add(securityMechanism);
    }

    public int getPoolNumber() {
        return poolNumber;
    }

    public void setPoolNumber(int poolNumber) {
        this.poolNumber = poolNumber;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }

    public void addAnnotation(Annotation annotation) {
        if (this.annotations == null) {
            this.annotations = new ArrayList<>();
        }
        this.annotations.add(annotation);
    }

    public static class CloudResource {
        private String providerId;
        private String zoneId;
        private String vmType;
        private String appliance;

        public String getProviderId() {
            return providerId;
        }

        public void setProviderId(String providerId) {
            this.providerId = providerId;
        }

        public String getZoneId() {
            return zoneId;
        }

        public void setZoneId(String zoneId) {
            this.zoneId = zoneId;
        }

        public String getVmType() {
            return vmType;
        }

        public void setVmType(String vmType) {
            this.vmType = vmType;
        }

        public String getAppliance() {
            return appliance;
        }

        public void setAppliance(String appliance) {
            this.appliance = appliance;
        }
    }

    public static class Allocation {
        private int poolSeqNum;
        private int vmSeqNum;
        private List<String> componentIds = new ArrayList<>();

        public Allocation() {
        }

        public Allocation(int poolSeqNum, int vmSeqNum, List<String> componentIds) {
            this.poolSeqNum = poolSeqNum;
            this.vmSeqNum = vmSeqNum;
            this.componentIds.addAll(componentIds);
        }

        public int getPoolSeqNum() {
            return poolSeqNum;
        }

        public void setPoolSeqNum(int poolSeqNum) {
            this.poolSeqNum = poolSeqNum;
        }

        public int getVmSeqNum() {
            return vmSeqNum;
        }

        public void setVmSeqNum(int vmSeqNum) {
            this.vmSeqNum = vmSeqNum;
        }

        public List<String> getComponentIds() {
            return componentIds;
        }

        public void setComponentIds(List<String> componentIds) {
            this.componentIds = componentIds;
        }

        public void addComponentId(String componentId) {
            this.componentIds.add(componentId);
        }
    }
}