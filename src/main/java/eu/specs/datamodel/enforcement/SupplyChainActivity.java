package eu.specs.datamodel.enforcement;

import eu.specs.datamodel.common.Annotation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SupplyChainActivity implements Serializable {
    private String id;
    private String slaId;
    private Date creationTime;
    private Status state;
    private List<CloudResources> cloudResources;
    private List<String> securityMechanisms;
    private List<Slo> slos;
    private List<Annotation> annotations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Status getState() {
        return state;
    }

    public void setState(Status state) {
        this.state = state;
    }

    public List<CloudResources> getCloudResources() {
        return cloudResources;
    }

    public void setCloudResources(List<CloudResources> cloudResources) {
        this.cloudResources = cloudResources;
    }

    public List<String> getSecurityMechanisms() {
        return securityMechanisms;
    }

    public void setSecurityMechanisms(List<String> securityMechanisms) {
        this.securityMechanisms = securityMechanisms;
    }

    public List<Slo> getSlos() {
        return slos;
    }

    public void setSlos(List<Slo> slos) {
        this.slos = slos;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }

    public void addAnnotation(Annotation annotation) {
        if (this.annotations == null) {
            this.annotations = new ArrayList<>();
        }
        this.annotations.add(annotation);
    }

    public static enum Status {
        CREATED,
        RUNNING,
        COMPLETED,
        ERROR
    }
}