package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DiagnosisMonEvent {

    @JsonProperty("dme_id")
    private String id;
    @JsonProperty("sla_id")
    private String slaId;
    @JsonProperty("slo_ids")
    private List<String> sloIds = new ArrayList<>();
    @JsonProperty("not_id")
    private String notificationId;
    @JsonProperty("measurement_time")
    private Date measurementTime;
    @JsonProperty("event_type")
    private String eventType;
    @JsonProperty("sla_impact")
    private int slaImpact;
    @JsonProperty("root_cause")
    private List<String> rootCauses = new ArrayList<>();
    @JsonProperty("priority_queue_timestamp")
    private Date priortyQueueTimestamp;
    @JsonProperty("creation_time")
    private Date creationTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public List<String> getSloIds() {
        return sloIds;
    }

    public void setSloIds(List<String> sloIds) {
        this.sloIds = sloIds;
    }

    public void addSloId(String sloId) {
        this.sloIds.add(sloId);
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public Date getMeasurementTime() {
        return measurementTime;
    }

    public void setMeasurementTime(Date measurementTime) {
        this.measurementTime = measurementTime;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public int getSlaImpact() {
        return slaImpact;
    }

    public void setSlaImpact(int slaImpact) {
        this.slaImpact = slaImpact;
    }

    public List<String> getRootCauses() {
        return rootCauses;
    }

    public void setRootCauses(List<String> rootCauses) {
        this.rootCauses = rootCauses;
    }

    public void addRootCause(String rootCause) {
        this.rootCauses.add(rootCause);
    }

    public Date getPriortyQueueTimestamp() {
        return priortyQueueTimestamp;
    }

    public void setPriortyQueueTimestamp(Date priortyQueueTimestamp) {
        this.priortyQueueTimestamp = priortyQueueTimestamp;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
