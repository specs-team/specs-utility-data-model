package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class SecurityMechanism {
    @JsonProperty("security_mechanism_id")
    private String id;
    @JsonProperty("security_mechanism_name")
    private String name;
    @JsonProperty("sm_description")
    private String description;
    @JsonProperty("security_capabilities")
    private List<String> securityCapabilities;
    @JsonProperty("enforceable_metrics")
    private List<String> enforceableMetrics;
    @JsonProperty("monitorable_metrics")
    private List<String> monitorableMetrics;
    @JsonProperty("measurements")
    private List<Measurement> measurements;
    @JsonProperty("metadata")
    private Metadata metadata;
    @JsonProperty("remediation")
    private Remediation remediation;
    @JsonProperty("chef_recipes")
    private List<ChefRecipe> chefRecipes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getSecurityCapabilities() {
        return securityCapabilities;
    }

    public void setSecurityCapabilities(List<String> securityCapabilities) {
        this.securityCapabilities = securityCapabilities;
    }

    public List<String> getEnforceableMetrics() {
        return enforceableMetrics;
    }

    public void setEnforceableMetrics(List<String> enforceableMetrics) {
        this.enforceableMetrics = enforceableMetrics;
    }

    public List<String> getMonitorableMetrics() {
        return monitorableMetrics;
    }

    public void setMonitorableMetrics(List<String> monitorableMetrics) {
        this.monitorableMetrics = monitorableMetrics;
    }

    public List<Measurement> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(List<Measurement> measurements) {
        this.measurements = measurements;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public Remediation getRemediation() {
        return remediation;
    }

    public void setRemediation(Remediation remediation) {
        this.remediation = remediation;
    }

    public List<ChefRecipe> getChefRecipes() {
        return chefRecipes;
    }

    public void setChefRecipes(List<ChefRecipe> chefRecipes) {
        this.chefRecipes = chefRecipes;
    }

    public static class Metadata implements Serializable {
        private static final long serialVersionUID = 4036254161193506555L;
        @JsonProperty("components")
        private List<Component> components;
        @JsonProperty("constraints")
        private List<Constraint> constraints;

        public List<Component> getComponents() {
            return components;
        }

        public void setComponents(List<Component> components) {
            this.components = components;
        }

        public List<Constraint> getConstraints() {
            return constraints;
        }

        public void setConstraints(List<Constraint> constraints) {
            this.constraints = constraints;
        }

        public static class Constraint implements Serializable {

            @JsonProperty("ctype")
            private String type;
            @JsonProperty("arg1")
            private List<String> arg1;
            @JsonProperty("arg2")
            private List<String> arg2;
            @JsonProperty("op")
            private String operator;
            @JsonProperty("n1")
            private String n1;
            @JsonProperty("n2")
            private String n2;

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public List<String> getArg1() {
                return arg1;
            }

            public void setArg1(List<String> arg1) {
                this.arg1 = arg1;
            }

            public List<String> getArg2() {
                return arg2;
            }

            public void setArg2(List<String> arg2) {
                this.arg2 = arg2;
            }

            public String getOperator() {
                return operator;
            }

            public void setOperator(String operator) {
                this.operator = operator;
            }

            public String getN1() {
                return n1;
            }

            public void setN1(String n1) {
                this.n1 = n1;
            }

            public String getN2() {
                return n2;
            }

            public void setN2(String n2) {
                this.n2 = n2;
            }
        }
    }

    public static class Remediation implements Serializable {
        /**
         *
         */
        private static final long serialVersionUID = -2945228109103553609L;
        @JsonProperty("remediation_actions")
        private List<RemAction> remActions;
        @JsonProperty("remediation_flow")
        private List<RemFlow> remFlows;

        public List<RemAction> getRemActions() {
            return remActions;
        }

        public void setRemActions(List<RemAction> remActions) {
            this.remActions = remActions;
        }

        public List<RemFlow> getRemFlows() {
            return remFlows;
        }

        public void setRemFlows(List<RemFlow> remFlows) {
            this.remFlows = remFlows;
        }
    }
}
