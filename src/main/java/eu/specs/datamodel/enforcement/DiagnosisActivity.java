package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.specs.datamodel.common.Annotation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DiagnosisActivity {
    @JsonProperty("diag_act_id")
    private String id;
    @JsonProperty("notification_id")
    private String notificationId;
    @JsonProperty("creation_time")
    private Date creationTime;
    @JsonProperty("state")
    private Status state;
    @JsonProperty("component")
    private String component;
    @JsonProperty("object")
    private String object;
    @JsonProperty("sla_id")
    private String slaId;
    @JsonProperty("plan_id")
    private String planId;
    @JsonProperty("measurement_id")
    private String measurementId;
    @JsonProperty("event_id")
    private String eventId;
    @JsonProperty("value")
    private String value;
    @JsonProperty("measurement_time")
    private Date measurementTime;
    @JsonProperty("affected_slos")
    private List<String> affectedSlos = new ArrayList<>();
    @JsonProperty("classification")
    private Classification classification;
    @JsonProperty("event_impact")
    private int eventImpact;
    @JsonProperty("root_cause")
    private List<String> rootCauses = new ArrayList<>();
    @JsonProperty("priority_queue_timestamp")
    private Date priorityQueueTimestamp;
    @JsonProperty("annotations")
    private List<Annotation> annotations = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Status getState() {
        return state;
    }

    public void setState(Status state) {
        this.state = state;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getMeasurementId() {
        return measurementId;
    }

    public void setMeasurementId(String measurementId) {
        this.measurementId = measurementId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getMeasurementTime() {
        return measurementTime;
    }

    public void setMeasurementTime(Date measurementTime) {
        this.measurementTime = measurementTime;
    }

    public List<String> getAffectedSlos() {
        return affectedSlos;
    }

    public void setAffectedSlos(List<String> affectedSlos) {
        this.affectedSlos = affectedSlos;
    }

    public void addAffectedSlo(String affectedSlo) {
        this.affectedSlos.add(affectedSlo);
    }

    public Classification getClassification() {
        return classification;
    }

    public void setClassification(Classification classification) {
        this.classification = classification;
    }

    public int getEventImpact() {
        return eventImpact;
    }

    public void setEventImpact(int eventImpact) {
        this.eventImpact = eventImpact;
    }

    public List<String> getRootCauses() {
        return rootCauses;
    }

    public void setRootCauses(List<String> rootCauses) {
        this.rootCauses = rootCauses;
    }

    public void addRootCause(String rootCause) {
        this.rootCauses.add(rootCause);
    }

    public Date getPriorityQueueTimestamp() {
        return priorityQueueTimestamp;
    }

    public void setPriorityQueueTimestamp(Date priorityQueueTimestamp) {
        this.priorityQueueTimestamp = priorityQueueTimestamp;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }

    public void addAnnotation(Annotation annotation) {
        this.annotations.add(annotation);
    }

    public static enum Status {
        RECEIVED,
        SLA_IDENTIFIED,
        CLASSIFIED,
        REMEDIATING,
        SOLVED,
        ERROR
    }

    public static enum Classification {
        FALSE_POSITIVE,
        ALERT,
        VIOLATION
    }
}
