package eu.specs.datamodel.enforcement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class MonitoringEvent {
    @JsonProperty("component")
    private String component;
    @JsonProperty("object")
    private String object;
    @JsonProperty("timestamp")
    private Date timestamp;
    @JsonProperty("labels")
    private List<String> labels;
    @JsonProperty("token")
    private String token;
    @JsonProperty("type")
    private String type;
    @JsonProperty("data")
    private String data;

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @JsonIgnore
    public String getSlaId() {
        if (labels != null) {
            for (String label : labels) {
                if (label.startsWith("sla_id_")) {
                    return label.substring("sla_id_".length());
                }
            }
        }

        return null;
    }

    @JsonIgnore
    public String getMetricId() {
        if (labels != null) {
            for (String label : labels) {
                if (label.startsWith("security_metric_")) {
                    return label.substring("security_metric_".length());
                }
            }
        }

        return null;
    }
}
