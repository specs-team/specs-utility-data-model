package eu.specs.datamodel.enforcement;

import eu.specs.datamodel.common.Annotation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ImplActivity {
    private String id;
    private String slaId;
    private String implPlanId;
    private String reconfigId;
    private String newPlanId;
    private Date creationTime;
    private Status state;
    private List<Annotation> annotations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlaId() {
        return slaId;
    }

    public void setSlaId(String slaId) {
        this.slaId = slaId;
    }

    public String getImplPlanId() {
        return implPlanId;
    }

    public void setImplPlanId(String implPlanId) {
        this.implPlanId = implPlanId;
    }

    public String getReconfigId() {
        return reconfigId;
    }

    public void setReconfigId(String reconfigId) {
        this.reconfigId = reconfigId;
    }

    public String getNewPlanId() {
        return newPlanId;
    }

    public void setNewPlanId(String newPlanId) {
        this.newPlanId = newPlanId;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Status getState() {
        return state;
    }

    public void setState(Status state) {
        this.state = state;
    }

    public List<Annotation> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        this.annotations = annotations;
    }

    public void addAnnotation(Annotation annotation) {
        if (this.annotations == null) {
            this.annotations = new ArrayList<>();
        }
        this.annotations.add(annotation);
    }

    public enum Status {
        CREATED,
        IMPLEMENTING,
        ACTIVE,
        ERROR
    }
}
