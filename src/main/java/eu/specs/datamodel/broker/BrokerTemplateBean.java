package eu.specs.datamodel.broker;

import com.google.gson.Gson;

public class BrokerTemplateBean {
    
    private String id;
    private String description;
    private String provider;
    private String zone;
    private String hw;
    private String appliance;
    
    
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getHw() {
		return hw;
	}

	public void setHw(String hw) {
		this.hw = hw;
	}

	public String getAppliance() {
		return appliance;
	}

	public void setAppliance(String appliance) {
		this.appliance = appliance;
	}
    
    public static BrokerTemplateBean create(String json){
        return new Gson().fromJson(json, BrokerTemplateBean.class);
    }
}