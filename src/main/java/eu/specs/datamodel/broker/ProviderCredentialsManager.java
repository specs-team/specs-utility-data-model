package eu.specs.datamodel.broker;

import java.util.HashMap;
import java.util.Map;


public class ProviderCredentialsManager {
	
	private static Map <String, ProviderCredential> map = new HashMap<String, ProviderCredential>();


	public static void add(String provider, ProviderCredential cred){
			map.put(provider, cred);	
	}
	
	 
	public static ProviderCredential getCredentials(String provider){
		return map.get(provider);
	}
	
}