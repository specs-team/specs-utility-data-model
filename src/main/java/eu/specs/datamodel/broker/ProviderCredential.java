package eu.specs.datamodel.broker;

public class ProviderCredential {
    
    private String username, password;
    
    public ProviderCredential(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}