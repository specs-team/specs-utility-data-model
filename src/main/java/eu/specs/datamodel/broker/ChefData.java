package eu.specs.datamodel.broker;

public class ChefData {
	private String chefServerUsername, chefUserString, chefUserPK;
	private String chefOrganizationPrivatKey, chefEndPoint;

	public String getChefEndPoint() {
		return chefEndPoint;
	}

	public void setChefEndPoint(String chefEndPoint) {
		this.chefEndPoint = chefEndPoint;
	}

	public String getChefServerUsername() {
		return chefServerUsername;
	}

	public void setChefServerUsername(String chefServerUsername) {
		this.chefServerUsername = chefServerUsername;
	}

	public String getChefUserPK() {
		return chefUserPK;
	}

	public void setChefUserPK(String chefUserPK) {
		this.chefUserPK = chefUserPK;
	}


	public String getChefOrganizationPrivatKey() {
		return chefOrganizationPrivatKey;
	}

	public void setChefOrganizationPrivatKey(String chefOrganizationPrivatKey) {
		this.chefOrganizationPrivatKey = chefOrganizationPrivatKey;
	}

	public String getChefUserString() {
		return chefUserString;
	}

	public void setChefUserString(String chefUserString) {
		this.chefUserString = chefUserString;
	}

}
