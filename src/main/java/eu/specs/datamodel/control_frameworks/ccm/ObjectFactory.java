
package eu.specs.datamodel.control_frameworks.ccm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the main.java.main.java.negotiation.eu.specs.negotiation.main.java.negotiation.eu.specs.main.java.negotiation.eu.specs.negotiation.control_frameworks package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SecurityControl_QNAME = new QName("http://www.specs-project.eu/resources/schemas/xml/control_frameworks/ccm", "security_control");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: main.java.main.java.negotiation.eu.specs.negotiation.main.java.negotiation.eu.specs.main.java.negotiation.eu.specs.negotiation.control_frameworks
     * 
     */
    public ObjectFactory() {
    }



    /**
     * Create an instance of {@link CCMSecurityControl }
     * 
     */
    public CCMSecurityControl createSecurityControl() {
        return new CCMSecurityControl();
    }

    @XmlElementDecl(namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/ccm", name = "security_control")
    public JAXBElement<String> createSecurityControl(String value) {
        return new JAXBElement<String>(_SecurityControl_QNAME, String.class, null, value);
    }

    @XmlElementDecl(namespace = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/ccm", name = "id")
    public JAXBElement<String> createId(String value) {
        return new JAXBElement<String>(_SecurityControl_QNAME, String.class, null, value);
    }




}
