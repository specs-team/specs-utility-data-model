package eu.specs.datamodel.common;

public enum SlaState {
    NEGOTIATING,
    SIGNED,
    OBSERVED,
    RENEGOTIATING,
    TERMINATING,
    TERMINATED,
    COMPLETE,
    ALERTED,
    VIOLATED,
    REMEDIATING,
    PROACTIVE_REDRESSING
}
