package eu.specs.datamodel.common;

import java.util.Date;

public class Annotation {
    private String name;
    private String value;
    private Date timestamp;

    public Annotation() {
    }

    public Annotation(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public Annotation(String name, String value, Date timestamp) {
        this.name = name;
        this.value = value;
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
