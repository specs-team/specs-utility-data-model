package eu.specs.datamodel.agreement.terms;

import eu.specs.datamodel.agreement.slo.ServiceProperties;

import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * Created by adispataru on 4/24/15.
 */
@XmlSeeAlso({ServiceDescriptionTerm.class, GuaranteeTerm.class, ServiceProperties.class, ServiceReference.class})
public abstract class Term {


}
