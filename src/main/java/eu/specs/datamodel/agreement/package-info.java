/**
 * Created by adispataru on 7/2/15.
 */
@XmlSchema(xmlns = {@XmlNs(prefix = "wsag", namespaceURI = "http://schemas.ggf.org/graap/2007/03/ws-agreement"),
                    @XmlNs(prefix = "nist", namespaceURI = "http://www.specs-project.eu/resources/schemas/xml/control_frameworks/nist"),
                    @XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance"),
                    @XmlNs(prefix = "xs", namespaceURI = "http://www.w3.org/2001/XMLSchema"),
                    @XmlNs(prefix = "specs", namespaceURI =  "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate")},
        elementFormDefault = XmlNsForm.QUALIFIED,
        namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate",
        location = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate SLAtemplate_NIST.xsd")
package eu.specs.datamodel.agreement;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;